class AddQualificationToLoad < ActiveRecord::Migration
  def change
    add_column :loads, :qualification, :float, :limit=>53
  end
end
