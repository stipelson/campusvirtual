class CreatePeriods < ActiveRecord::Migration
  def change
    create_table :periods do |t|
      t.date :start
      t.date :end
      t.integer :year
      t.string :semester
      t.boolean :enabled

      t.timestamps null: false
    end
  end
end
