class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.text :body
      t.boolean :feactured, :default => false, :null => false
      t.text :abstract
      t.string :color

      t.timestamps null: false
    end
  end
end
