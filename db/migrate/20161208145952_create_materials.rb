class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.references :subject, index: true, foreign_key: true
      t.string :name
      t.string :attachment

      t.timestamps null: false
    end
  end
end
