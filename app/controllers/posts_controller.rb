class PostsController < ApplicationController
    load_and_authorize_resource only: [:create, :new]

    def index
        @posts = Post.order('created_at DESC').page params[:page]
    end

    def new
        @post = Post.new
    end

    def create
        @post = Post.new(post_params)
        @post.user = current_user
        if @post.save
            flash[:notice] = "Successfully created post!"
            redirect_to posts_path
        else
            flash[:alert] = "Error creating new post!"
            render :new
        end
    end

    private

    def post_params
        params.require(:post).permit(:title, :body)
    end
end
