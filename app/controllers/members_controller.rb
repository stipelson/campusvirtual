class MembersController < ApplicationController
  before_action :find_user,   only: [:edit_membership, :update]

  def index
    if params[:approved] == "false"
      @users = User.where(approved: false).page params[:page]
      @all_user = false;
    else
      @users = User.page params[:page]
      @all_user = true;
    end

    authorize! :read, @users
    @countAll = User.count

  end

  def edit_membership
  end

  def apiusers
    @users = User.all.collect {|p| [ p.email, 'null' ] }.to_h
    respond_to do |format|
      format.json { render :json => @users, :status => 200 }
      format.html { render :json => @users, :status => 200 }
    end
  end

  def teachers
    @users = User.joins(:roles).where(:roles => {:name => 'profesor'}).collect {|p| [ p.email, 'null' ] }.to_h
    respond_to do |format|
      format.json { render :json => @users, :status => 200 }
      format.html { render :json => @users, :status => 200 }
    end
  end

  def update
    if @user.update_attributes(member_params)
      @user.roles <<  Role.find(params[:role_ids]) if params[:role_ids]
      @user.save
      redirect_to member_edit_membership_path(@user), :notice => "Membership updated"
    else
      render 'edit_membership'
    end
  end

  def qualifications
    authorize! :qualifications, User
  end

  # Confirms the correct user.
  def find_user
    @user = User.find(params[:id])
  end

  def member_params
    params.require(:user).permit(:approved, :role_ids => [])
  end

end
