class NewsController < ApplicationController
    before_action :find_news, only: [:edit, :update, :show, :delete]
    load_and_authorize_resource

    def index
        if params[:highlighted] == "true"
            @news = News.where(highlighted: true).order(:created_at).page params[:page]
            @all_news = false;
        else
            @news = News.order('created_at DESC').page params[:page]
            @all_news = true;
        end
        @countAll = News.count
    end

    def new
        @news = News.new
    end

    def create
        @news = News.new(news_params)
        @news.user = current_user
        if @news.save
            flash[:notice] = "Successfully created piece of news!"
            redirect_to news_index_path
        else
            flash[:alert] = "Error creating new piece of news!"
            render :new
        end
    end

    def edit
    end

    def update
        if @news.update_attributes(news_params)
            flash[:notice] = "Successfully updated piece of news!"
            redirect_to news_index_path
        else
            flash[:alert] = "Error updating piece of news!"
            render :edit
        end
    end

    def show
    end

    def destroy
        if @news.destroy
            flash[:notice] = "Successfully deleted piece of news!"
            redirect_to news_index_path
        else
            flash[:alert] = "Error updating new!"
        end
    end

    private

    def news_params
        params.require(:news).permit(:title, :body, :highlighted, :color, :feactured, :image)
    end

    def find_news
        @new = News.find(params[:id])
    end
end
