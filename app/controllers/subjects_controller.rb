class SubjectsController < ApplicationController
    before_action :find_subject,  only: [:show, :update, :destroy, 'participants']
    load_and_authorize_resource
    
    def index
        if current_user.role?('super_admin')
            @subjects = Subject.order('created_at DESC').where(period_id: params[:period_id]).page params[:page]
        else
            @subjects = current_user.subjects.order('created_at DESC').where(period_id: params[:period_id]).page params[:page]
        end
        @period = Period.find(params[:period_id])
    end

    def new
        @subject = Subject.new
        @period = Period.find(params[:period_id])
    end

    def show
        @enabled = true
        if @subject.period.end < Date.today
            @enabled = false
        end
        @material = Material.new

        @loadCharge = @subject.loads.where(:user_id => current_user.id)

        if @loadCharge.any?
            @qualification = @loadCharge.first.qualification
        end
    end

    def participants
        @enabled = true
        if @subject.period.end < Date.today
            @enabled = false
        end
        @loads = Load.where(subject_id: params[:id]).page params[:page]
        @load = Load.new
    end
    
    def create
        @subject = Subject.new(subject_params)
        @period = Period.find(params[:period_id])
    
        if @user = User.where(email: params[:teacher]).first
            @load = Load.new
            @load.subject = @subject
            @load.user = @user
            @load.role = Role.where(name: 'profesor').first
            if !@load.save
                flash[:alert] = "Error creating new subject!"
                render :new and return           
            end
        end

        @subject.period = @period

        if @subject.save
            flash[:notice] = "Successfully created subject!"
            redirect_to period_subjects_path(@period)
        else
            flash[:alert] = "Error creating new subject!"
            render :new
        end
    end

    private

    def find_subject
        @subject = Subject.find(params[:id])
        @period = Period.find(@subject.period_id)
    end

    def subject_params
        params.require(:subject).permit(:name, :code)
    end

end
