class MaterialsController < ApplicationController
    load_and_authorize_resource
    
    def new
        @subject = Subject.find(params[:subject_id])
        @period = @subject.period
        @material = Material.new
    end

    def create
        @subject = Subject.find(params[:subject_id])
        if @subject.period.end >= Date.today
            @material = Material.new(material_params)
            @material.subject = @subject

            if @material.save
                redirect_to subject_path(@subject), notice: "The material #{@material.name} has been uploaded."
            else
                flash[:alert] = "Error upload!"
                redirect_to subject_path(@subject) and return
            end
        else
            redirect_to subject_path(params[:subject_id]), alert:  "The material #{@material.name} not has been uploaded. The period is over."
        end
    end

    def destroy
        @subject = Subject.find(params[:subject_id])
        if @subject.period.end >= Date.today
            @material = Material.find(params[:id])
            @material.destroy
            redirect_to subject_path(params[:subject_id]), notice:  "The material #{@material.name} has been deleted."
        else
            redirect_to subject_path(params[:subject_id]), alert:  "The material #{@material.name} not has been deleted. The period is over."
        end
        
    end

    private
    def material_params
        params.require(:material).permit(:name, :attachment)
    end
end
