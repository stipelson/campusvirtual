class LoadsController < ApplicationController
    before_action :find_subject,  only: [:new, :create]
    load_and_authorize_resource

    def new
        
    end

    def create
        @subject = Subject.find(params[:subject_id])

        if @subject.period.end < Date.today
            flash[:alert] = "The user does not added. The period is over!"
            redirect_to subject_users_path(@subject) and return
        end

        @user = User.where(email: params[:user_email]).first

        if !@user.role_id?(params[:load][:role_id])
            flash[:alert] = "The user does not have this role!"
            redirect_to subject_users_path(@subject) and return
        end

        #@role = Role.find_by_name('profesor')
        #if @subject.users.where(:loads => {:user_id => current_user.id, :role_id => @role.id }).count <= 0
        #    flash[:alert] = "The user does not have the role teacher!"
        #    redirect_to subject_users_path(@subject) and return
        #end

        @loadFind = Load.where(user_id: @user.id, subject_id: @subject.id)

        if @loadFind.count <= 0
            
            @load = Load.new(load_params)
            @load.subject = @subject
            @load.user = @user

            if @load.save and @load.save
                flash[:notice] = "Successfully add user!" 
                redirect_to subject_users_path(@subject) and return
            else
                flash[:alert] = "Error add user!"
                redirect_to subject_users_path(@subject) and return
            end
        else
            flash[:alert] = "This user is already added!"
            redirect_to subject_users_path(@subject) and return
        end

    end

    def update
        @load = Load.find(params[:id])
        if @load.subject.period.end < Date.today
            flash[:alert] = "Error. The period is over!"
            redirect_to subject_users_path(@load.subject) and return
        end

        if @load.update_attributes(load_params)
            flash[:notice] = "Successfully edit!" 
            redirect_to subject_users_path(@load.subject) and return
        else
            flash[:alert] = "Error!"
            subject_users_path(@load.subject) and return
        end 
    end

    private

    def find_subject
        @subject = Subject.find(params[:subject_id])
    end

    def load_params
        params.require(:load).permit(:subject, :role_id, :qualification)
    end

end
