class PeriodsController < ApplicationController
    load_and_authorize_resource

    def index
        @periods = Period.order('created_at DESC').page params[:page]
    end

    def list
        @periods = Period.order('created_at DESC').page params[:page]
    end

    def new
        @period = Period.new
    end

    def create
        @period = Period.new(period_params)
        @period.enabled = true
        if @period.save
            flash[:notice] = "Successfully created period!"
            redirect_to periods_path
        else
            flash[:alert] = "Error creating new period!"
            render :new
        end
    end

    private

    def period_params
        params.require(:period).permit(:year, :semester, :start, :end)
    end

end
