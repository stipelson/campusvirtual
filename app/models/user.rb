class User < ActiveRecord::Base
  paginates_per 10
  has_many :news
  has_and_belongs_to_many :roles 
  accepts_nested_attributes_for :roles
  has_many :loads
  has_many :subjects, through: :loads
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

    def role?( role ) 
        !roles.find_by_name( role ).nil?
    end 

    def role_id?( role ) 
        !roles.find_by_id( role ).nil?
    end 

    def active_for_authentication? 
        super && approved? 
    end

    def subjectLoad?(subject)
        !loads.find_by_subject_id(subject).nil?
    end

    def inactive_message 
        if !approved? 
          :not_approved 
        else 
            super # Use whatever other message 
        end 
    end
    
end
