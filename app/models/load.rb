class Load < ActiveRecord::Base
  belongs_to :subject
  belongs_to :user
  belongs_to :role
end
