class Subject < ActiveRecord::Base
  belongs_to :period
  has_many :loads
  has_many :materials
  has_many :users, :through => :loads

  def haveUser?(user)
    !users.find_by_id(user).nil?
      
  end

  def qualification(user)
      loads.where('loads.user_id == '+ user.to_s ).first.qualification
  end
end
