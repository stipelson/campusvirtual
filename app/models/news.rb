class News < ActiveRecord::Base
    paginates_per 4
    belongs_to :user
    mount_uploader :image, ImageUploader # Tells rails to use this uploader for this model.
end
