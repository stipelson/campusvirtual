Rails.application.routes.draw do
  get 'materials/index'

  get 'materials/new'

  get 'materials/create'

  get 'materials/destroy'

  scope "(:locale)", locale: /es|en/ do

    #ruta para editar una membresia
    get 'member/:id/edit_membership' => 'members#edit_membership', as: :member_edit_membership
    #get 'members/unapproved' => 'members#members_unapproved', as: :members_unapproved

    # ruta para agregar un rol a un usuario

    resources :members do
      member do
        put :add_role
      end
    end

    #rutas para las noticias 

    resources :news
    resources :posts
    resources :subjects do
      resources :loads, only: [:index, :new, :create]
      resources :materials, only: [:index, :new, :create, :destroy]
    end
    resources :loads, only: [:update]

    resources :periods do
      resources :subjects, only: [:index, :new, :create]
    end

    resources :subjects, only: [:show, :edit, :update, :destroy]
    get '/subjects/:id/participants', to: 'subjects#participants', as: 'subject_users'

    get '/periods_list', to: 'periods#list', as: 'periods_list'
    get '/teachers', to: 'members#teachers', as: 'members_teachers'
    get '/apiusers', to: 'members#apiusers', as: 'api_users'
    get '/members/:user_id/qualifications', to: 'members#qualifications', as: 'member_qualifications'

    devise_for :users

  end

  root 'welcome#index'
  get '/:locale' => 'welcome#index'
  

  # The priority is based upon order of creation: first created -> highest priority.
    # See how all your routes lay out with "rake routes".

    # You can have the root of your site routed with "root"

    
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
