##Campus virtual, Proyecto 2 WWW

#####Realizado con:
- Ruby 2.3.1.
- Gema Rails 4.2.6.
- Gema [Devise](https://github.com/plataformatec/devise).
- Gema [CanCan](https://github.com/ryanb/cancan).

#####Dependencias extra necesarias:
- [Bower](https://bower.io/) 

#####Configuracion base de datos con Sqlite3:

######Archivo Gemfile
```ruby
# comentar la gema de mysql2
#gem 'mysql2' 
#agregar la gema de sqlite3
gem 'sqlite3'
```

######Archivo config/database.yml
```ruby
# SQLite version 3.x
#   gem install sqlite3
#
#   Asegurese de que la gema SQLite 3 esta definida en su Gemfile
#   gem 'sqlite3'
#
default: &default
  adapter: sqlite3
  pool: 5
  timeout: 5000

development:
  <<: *default
  database: db/development.sqlite3

# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  <<: *default
  database: db/test.sqlite3

production:
  <<: *default
  database: db/production.sqlite3
```

#####Configuracion base de datos con MySQL2:

######Archivo Gemfile
```ruby
# gema de mysql2
gem 'mysql2' 
#comentar o quitar la gema de sqlite3
#gem 'sqlite3'
```

######Archivo config/database.yml

```ruby
default: &default
  adapter: mysql2
  encoding: utf8
  username: root
  password: password #contraseña de la BD
  host: 127.0.0.1  # o localhost
  port: 3306
  pool: 5
  timeout: 5000

development:
  <<: *default  
  database: db_campusvirtual
  

# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  <<: *default
  database: db_test

production:
  <<: *default
  database: db_production
```

#####Comandos para poder correr el proyecto:

######En la terminal correr:

```ruby
bundle install
bower update
rake db:create
rake db:migrate
rails s
```

######Navegar a: http://localhost:3000
